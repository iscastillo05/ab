<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function index()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = new User();
        $user->setUsername('isidro');
        $user->setPassword('castillo');
        $entityManager->persist($user);

        $entityManager->flush();
        return new Response('Saved new user with id: ' . $user->getId());

//        return $this->render('login/index.html.twig', [
//            'controller_name' => 'LoginController',
//        ]);
    }

    /**
     * @Route("/user/{id}", name="login")
     */
    public function mostrarUsuarios($id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        if (!$user) {
            throw $this->createNotFoundException('No user found for id: ' . $id);
        }
        return new Response('name user: ' . $user->getUsername());
    }

    /**
     * @Route("/user/vista-nueva", name="show_user")
     */
    public function mostrar($id)
    {
        return new Response('name user: ');
    }
}